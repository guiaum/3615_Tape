/**
 * Getting Started with Capture.
 * 
 * Reading and displaying an image from an attached Capture device. 
 */
import processing.serial.*;
import processing.video.*;

import oscP5.*;
import netP5.*;

PShape overlay;
PShape play;

OscP5 oscP5;
NetAddress myRemoteLocation;

boolean serialTried = false;

Capture cam;
Serial myPort;  // Create object from Serial class
int val;
//Zone de détection
int zone = 200;

//Pour stocker les marquers détectées
ArrayList <PVector> myDetectedArucos = new ArrayList();

//Lissage des détections
int theNberOfChanges[] = new int[16];
int stateArucos[] = new int[16];
String convertedStateAruco;
String previousStateAruco;
int seuil = 50;



void setup() {
  size(800, 500);
  frameRate(20);
  String[] cameras = Capture.list();

  if (cameras == null) {
    println("Failed to retrieve the list of available cameras, will try the default...");
    //cam = new Capture(this, 320, 260, 30);
  } 
  if (cameras.length == 0) {
    println("There are no cameras available for capture.");
    exit();
  } else {
    println("Available cameras:");
    //printArray(cameras);
    cam = new Capture(this, 640, 500, 20);

    // Start capturing the images from the camera
    cam.start();
    delay(1000);

    // Connection série
    String portName = Serial.list()[0];
    printArray(Serial.list());
    myPort = new Serial(this, portName, 9600);
    delay(1000);

    myPort.bufferUntil(0x0a);

    oscP5 = new OscP5(this, 9002);
    myRemoteLocation = new NetAddress("192.168.1.93", 9001);

    noFill();
    stroke(255, 0, 0);
    rectMode(CENTER);
    ellipseMode(CENTER);

    //Init Arucos Array
    for (int j=0; j<16; j++)
    {
      stateArucos[j] = 0;
      previousStateAruco += 0;
    }
  }
  overlay = loadShape("overlay.svg");
  play = loadShape("play.svg");
}

void draw() {
  background(0);
  if (cam.available() == true) {
    cam.read();
  }
  //Affichage de l'image de la caméra
  image(cam, 80, 0, 640, 500);
  // Viseur
  noFill();
  stroke(255);
  strokeWeight(4);
  rect(width/2, height/2, zone, zone);

  // Dessin des marqueurs détectés
  for (int i=0; i<myDetectedArucos.size(); i++)
  {
    fill(255);
    pushMatrix();
    translate(width/2, height/2);
    shape(play, myDetectedArucos.get(i).x-21, myDetectedArucos.get(i).y-29, 43, 59);
    popMatrix();
  }
  myDetectedArucos.clear();

  // Calcul du trig ou non
  for (int j=0; j<16; j++)
  {
    theNberOfChanges[j]=(2*theNberOfChanges[j])/3;
    if (theNberOfChanges[j]<seuil)
    {
      stateArucos[j]=0;
    } else {
      stateArucos[j]=1;
    }
    convertedStateAruco += stateArucos[j] ;
  }

  if (convertedStateAruco.equals(previousStateAruco) == false)
  {
    println("previousStateAruco=> " + previousStateAruco);
    println("stateAruco=> " + convertedStateAruco);
    println("changed states, we trig");
    sendOSCMessage();
    previousStateAruco = convertedStateAruco;
  }
  convertedStateAruco = "";
  
  shape(overlay, 0,0);
}


void serialEvent(Serial p)
{
  try {
    String inBuffer = p.readString();
    if (inBuffer.charAt(0) == 'N')
    {
      String[] infoList = split(inBuffer, ' ');

      // Convert X and Y to int
      int theX = int(infoList[2]);
      int theY = int(infoList[3]);

      // Conversion dans le bon ratio
      float themappedX = map(theX, -1000, 1000, -320, 320);
      float themappedY = map(theY, -750, 750, -240, 240);

      // Test pour voir s'ils sont dans la zone
      if (abs(themappedX) < zone/2 && abs(themappedY) < zone/2)
      {
        // On stocke X et Y pour les dessiner
        PVector myDetectedAruco = new PVector (themappedX, themappedY);
        myDetectedArucos.add(myDetectedAruco);

        // On repere l'ID
        String theID = infoList[1].substring(1);
        int theIDint = int(theID);
        //println("TheID=> "+theID);
        //On ajoute une valeur au score du bidule
        theNberOfChanges[theIDint-1] += 100;
      }
    }
  } 
  catch (Exception e) {
    println (e);
  }
}



void sendOSCMessage()
 {
 
 OscMessage myMessage = new OscMessage("/track");
 //To change 1/2/3
 myMessage.add(1);
 myMessage.add(convertedStateAruco);
 oscP5.send(myMessage, myRemoteLocation);
 //print("Sent Message: 1 "+ myTracks);
 //println();
 }