import oscP5.*;
import netP5.*;

OscP5 oscP5;
NetAddress myRemoteLocation;

import controlP5.*;

ControlP5 cp5;

CheckBox checkbox1;
CheckBox checkbox2;
CheckBox checkbox3;

void setup() 
{
  /*
  String portName = "/dev/ttyUSB0";
   myPort = new Serial(this, portName, 9600);
   */
  oscP5 = new OscP5(this, 9002);
  myRemoteLocation = new NetAddress("192.168.1.93", 9001);

  size(500, 150);
  smooth();
  cp5 = new ControlP5(this);

  checkbox1 = cp5.addCheckBox("checkBox1")
    .setPosition(20, 20)
    .setSize(20, 20)
    .setItemsPerRow(4)
    .setSpacingColumn(10)
    .setSpacingRow(10)
    .addItem("0", 0)
    .addItem("1", 1)
    .addItem("2", 2)
    .addItem("3", 3)
    .addItem("4", 4)
    .addItem("5", 5)
    .addItem("6", 6)
    .addItem("7", 7)
    .addItem("8", 8)
    .addItem("9", 9)
    .addItem("10", 10)
    .addItem("11", 11)
    .addItem("12", 12)
    .addItem("13", 13)
    .addItem("14", 14)
    .addItem("15", 15)
    ;

  checkbox2 = cp5.addCheckBox("checkBox2")
    .setPosition(200, 20)
    .setSize(20, 20)
    .setItemsPerRow(4)
    .setSpacingColumn(10)
    .setSpacingRow(10)
    .addItem("16", 0)
    .addItem("17", 1)
    .addItem("18", 2)
    .addItem("19", 3)
    .addItem("20", 4)
    .addItem("21", 5)
    .addItem("22", 6)
    .addItem("23", 7)
    .addItem("24", 8)
    .addItem("25", 9)
    .addItem("26", 10)
    .addItem("27", 11)
    .addItem("28", 12)
    .addItem("29", 13)
    .addItem("30", 14)
    .addItem("31", 15)
    ;

  checkbox3 = cp5.addCheckBox("checkBox3")
    .setPosition(370, 20)
    .setSize(20, 20)
    .setItemsPerRow(4)
    .setSpacingColumn(10)
    .setSpacingRow(10)
    .addItem("32", 0)
    .addItem("33", 1)
    .addItem("34", 2)
    .addItem("35", 3)
    .addItem("36", 4)
    .addItem("37", 5)
    .addItem("38", 6)
    .addItem("39", 7)
    .addItem("40", 8)
    .addItem("41", 9)
    .addItem("42", 10)
    .addItem("43", 11)
    .addItem("44", 12)
    .addItem("45", 13)
    .addItem("46", 14)
    .addItem("47", 15)
    ;
}

void draw()
{
  background(170);
}

void keyPressed() {
  if (key=='1') {
    checkbox1.deactivateAll();
  } 
  if (key=='2') {
    checkbox2.deactivateAll();
  }
  if (key=='3') {
    checkbox3.deactivateAll();
  }
}

void controlEvent(ControlEvent theEvent) {

  if (theEvent.isFrom(checkbox1)) {
    //print("got an event from "+checkbox1.getName()+"\t\n");
    // checkbox uses arrayValue to store the state of 
    // individual checkbox-items. usage:

    //println(checkbox1.getArrayValue());
    String myTracks = "";
    for (int i=0; i<checkbox1.getArrayValue().length; i++) {
      int n = (int)checkbox1.getArrayValue()[i];

      myTracks += n;
    }

    OscMessage myMessage = new OscMessage("/track");
    myMessage.add(1);
    myMessage.add(myTracks);
    oscP5.send(myMessage, myRemoteLocation);
    print("Sent Message: 1 "+ myTracks);
    println();
  } else if (theEvent.isFrom(checkbox2)) {
    //print("got an event from "+checkbox1.getName()+"\t\n");
    // checkbox uses arrayValue to store the state of 
    // individual checkbox-items. usage:

    //println(checkbox1.getArrayValue());
    String myTracks = "";
    for (int i=0; i<checkbox2.getArrayValue().length; i++) {
      int n = (int)checkbox2.getArrayValue()[i];

      myTracks += n;
    }

    OscMessage myMessage = new OscMessage("/track");
    myMessage.add(2);
    myMessage.add(myTracks);
    oscP5.send(myMessage, myRemoteLocation);
    print("Sent Message: 2 "+ myTracks);
    println();
  } else  if (theEvent.isFrom(checkbox3)) {
    //print("got an event from "+checkbox1.getName()+"\t\n");
    // checkbox uses arrayValue to store the state of 
    // individual checkbox-items. usage:

    //println(checkbox1.getArrayValue());
    String myTracks = "";
    for (int i=0; i<checkbox3.getArrayValue().length; i++) {
      int n = (int)checkbox3.getArrayValue()[i];

      myTracks += n;
    }

    OscMessage myMessage = new OscMessage("/track");
    myMessage.add(3);
    myMessage.add(myTracks);
    oscP5.send(myMessage, myRemoteLocation);
    print("Sent Message: 3 "+ myTracks);
    println();
  }
}

void checkBox(float[] a) {
  println(a);
}