import toxi.geom.*;
import toxi.processing.*;

class ColoredLine2D extends Line2D {
  
  String theColor;
  
  ColoredLine2D(Vec2D point1, Vec2D point2, String sTheColor){
    super (point1, point2);
    theColor = sTheColor;
  }
  
  String getColor()
  {
   return theColor; 
  }
  
  
}