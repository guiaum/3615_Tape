import toxi.geom.*;
import toxi.processing.*;

class ColoredTriangle2D extends Triangle2D {
  
  String theColor;
  
  ColoredTriangle2D(Vec2D point1, Vec2D point2, Vec2D point3, String sTheColor){
    super (point1, point2, point3);
    theColor = sTheColor;
  }
  
  String getColor()
  {
   return theColor; 
  }
  
  
}