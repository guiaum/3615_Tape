import toxi.geom.*;
import toxi.processing.*;
import processing.pdf.*;
import java.util.Collections;
import java.util.Map;
import processing.serial.*;

// TO DO
// Jouer avec les valeurs de décalage d'outils en GCode
// pour les différents crayons

boolean debug = false;
boolean exportPDF = true;

//Settings
float densityOfLines = 10;
//nber of iterations of the merge routine
int iter = 5;

//Dimensions
int cellWidth = 150;
int marginLeft = 50;
int marginTop = 50;
int innerMargin = 10;

PGraphicsPDF pdf;

///////////////////////////////////////
// Arucos markers

int[] myAruco1  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 0, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco2  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 1, 1, 0, 
  0, 0, 0, 1, 1, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 1, 1, 0, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco3  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 0, 1, 0, 0, 0, 
  0, 0, 1, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco4  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 1, 0, 1, 0, 
  0, 0, 1, 0, 0, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco5  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 1, 1, 1, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 1, 0, 0, 0, 
  0, 1, 1, 0, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco6  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco7  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 0, 0, 0, 
  0, 0, 1, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco8  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 1, 1, 0, 1, 0, 
  0, 1, 0, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco9  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 0, 1, 0, 1, 0, 
  0, 0, 1, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco10  = {
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 1, 0, 0, 1, 0, 
  0, 0, 0, 0, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco11  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 1, 0, 
  0, 0, 0, 0, 1, 0, 
  0, 1, 0, 1, 0, 0, 
  0, 0, 1, 1, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco12  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 1, 0, 1, 1, 0, 
  0, 0, 1, 1, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco13  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 1, 0, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
  0, 1, 1, 1, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};


int[] myAruco14  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 0, 1, 0, 0, 0, 
  0, 1, 0, 1, 1, 0, 
  0, 0, 0, 0, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco15  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 0, 1, 0, 0, 
  0, 0, 1, 1, 0, 0, 
  0, 0, 0, 1, 1, 0, 
  0, 1, 1, 1, 0, 0, 
  0, 0, 0, 0, 0, 0, 
};

int[] myAruco16  = {
  0, 0, 0, 0, 0, 0, 
  0, 0, 1, 0, 0, 0, 
  0, 0, 1, 1, 0, 0, 
  0, 0, 1, 1, 0, 0, 
  0, 0, 1, 0, 1, 0, 
  0, 0, 0, 0, 0, 0, 
};

///////////////////////////////////////

float partDim;
float deltaLines;

// Everything to store datas
ArrayList<Line2D> pixelLinesH = new ArrayList<Line2D>();
ArrayList<Line2D> pixelLinesV = new ArrayList<Line2D>();
ArrayList<ColoredTriangle2D> pixelLinesMerged = new ArrayList<ColoredTriangle2D>();
ArrayList<ColoredLine2D> pixelLinesFinal = new ArrayList<ColoredLine2D>();
HashMap<String, Integer> pens = new HashMap<String, Integer>();
HashMap<String, Vec2D> penOffsets = new HashMap<String, Vec2D>();
ArrayList <String> indexOfPens = new ArrayList<String>();
ArrayList <int[]> myArucos = new ArrayList();
ArrayList <Integer> myPens = new ArrayList();

/////////////////////////////////////////

Serial myPort;                       // The serial port
boolean connectionWithGRBL = false;

// PARAMS
String outputFolder = "./data/";
String outputFile = "bufferGCode";

int paperWidth = 700;
int paperHeight = 700;

// Paper sheet dimensions (in mm in real life)
int pW = paperWidth;
int pH = paperHeight;

float scaleRatio = 1.0;

// Defines an output file
PrintWriter output;
boolean GCodeExported = false;

// Define pen UP and DOWN positions
// TO CHANGE WITH COLORS
float penUp = 1.0f;
float penDown = 0.0f;
String Pen1 = "M4";
String Pen2 = "M3";
boolean whichPen = true;
int servoUp = 255;
int servoDown = 0;
boolean isPenDown = false; // used in order not to repeat unnecessarily the penDown command

// Define Feed rate (stepper motors speed)
float motorFeedSlow = 5000.0f;
float motorFeedFast = 7000.0f;

// Used for reading the generated GCode
String[] lines;
int index = 0;

boolean isStreamingGcode = false;
boolean firstContact = true;

int state = 0;

char previousChar;


void setup()
{
  size(700, 700, P2D);
  partDim = (cellWidth-(innerMargin*2))/6;
  deltaLines = partDim/densityOfLines;
  ellipseMode(CENTER);

  if (exportPDF) pdf = (PGraphicsPDF)beginRecord(PDF, "Arucos.pdf");

  myArucos.add(myAruco1);
  myArucos.add(myAruco2);
  myArucos.add(myAruco3);
  myArucos.add(myAruco4);
  myArucos.add(myAruco5);
  myArucos.add(myAruco6);
  myArucos.add(myAruco7);
  myArucos.add(myAruco8);
  myArucos.add(myAruco9);
  myArucos.add(myAruco10);
  myArucos.add(myAruco11);
  myArucos.add(myAruco12);
  myArucos.add(myAruco13);
  myArucos.add(myAruco14);
  myArucos.add(myAruco15);
  myArucos.add(myAruco16);

  pens.put( "red", color(247, 30, 30));
  pens.put(  "green", color(155, 232, 89));
  pens.put(  "blue", color(53, 164, 196));
  pens.put(  "black", color(0, 0, 0));

  //RED
  penOffsets.put("red", new Vec2D(41, 17));
  //GREEN
  penOffsets.put("green", new Vec2D(-44, -15)); 
  //BLUE
  penOffsets.put("blue", new Vec2D(-49, -20)); 
  //BLACK
  penOffsets.put("black", new Vec2D(40, -15));

  indexOfPens.add("red");
  indexOfPens.add("green");
  indexOfPens.add("blue");
  indexOfPens.add("black");

  /////// CONNECTION MANUELLE
  printArray(Serial.list());
  try {
    myPort = new Serial(this, "/dev/ttyUSB0", 115200);
  }
  catch(Exception e) {
    e.printStackTrace();
  }
  delay(5000);
  //Initialisation du plotter  
  //UNLOCK
  myPort.write("$X" + "\n");
  delay(1000);
  //ALL PENS UP
  myPort.write("M4 S0" + "\n");
  delay(1000);
  //RESET ZERO
  myPort.write("G10 P0 L20 X0 Y0 Z0" + "\n");
  delay(1000);
}


void draw()
{
  background (255);

  switch (state)
  { 
  case 0:
    //////////////////////////////////////////////////
    // GENERATION DES LIGNES
    // On dessine chaque marqueur à partir du bas droite
    //////////////////////////////////////////////////
    if (hour()>=7 && hour()<= 21)
    {
      Collections.shuffle(myArucos);
      for (int i=myArucos.size()-1; i>-1; i--)
      {

        int[] drawnAruco = myArucos.get(i);
        int originX = (i%4)*cellWidth + marginLeft;
        int originY = ceil((i)/4)*cellWidth + marginTop;

        for (int j=0; j<drawnAruco.length; j++)
        {

          int x = j%6;
          int y = ceil(j/6);
          //println("j "+j+" x "+x+" y "+y);

          if (debug)
          {
            if (drawnAruco[j] == 0)
            { 
              fill(0);
            } else {
              fill (255);
            }
            noStroke();
          }

          float partX = x*partDim + originX + innerMargin;
          float partY = y*partDim + originY + innerMargin;

          if (debug)
          {
            rect(partX, partY, partDim, partDim);
          }
          // Horiz Lines
          stroke(0, 255, 0);
          for (int xH = 0; xH<densityOfLines; xH ++)
          {
            if (drawnAruco[j] == 0)
            {
              Line2D rawLine;
              rawLine = new Line2D(new Vec2D(partX, partY + xH*deltaLines), new Vec2D(partX + partDim, partY +xH*deltaLines));
              pixelLinesH.add(rawLine);
            }
          }
          mergePixelLinesH();

          // Vert Lines
          stroke(255, 0, 0);
          for (int xH = 0; xH<densityOfLines; xH ++)
          {
            if (drawnAruco[j] == 0)
            {
              Line2D rawLine;
              rawLine = new Line2D(new Vec2D(partX + xH*deltaLines, partY), new Vec2D(partX + xH*deltaLines, partY + partDim));
              pixelLinesV.add(rawLine);
            }
          }
        }
        mergePixelLinesV();

        // CONVERSION DES LIGNES POUR ADAPTER AU D2CALAGE DES STYLOS
        Collections.shuffle(indexOfPens);
        populatePixelLinesMerged(indexOfPens.get(0), indexOfPens.get(1));

        //drawPixelLinesH(indexOfPens.get(0));
        //drawPixelLinesV(indexOfPens.get(1));

        //if (exportPDF) pdf.nextPage();

        pixelLinesH.clear();
        pixelLinesV.clear();
      }
      // PREPARATION AVANT GCODE
      mergeAllPixelLines();
      drawAllPixelLines();
      
      if (exportPDF)endRecord();
      //if (exportPDF)exit();
       
      delay(500);
      state = 1;
      //noLoop();
    }
    break;


  case 1:
    //////////////////////////////////////////////////
    //Xport to GCODE
    //////////////////////////////////////////////////
    output = createWriter(outputFolder + outputFile +".ngc");
    GCodeInit();
    GCodeWrite();
    state = 2;
    break;

  case 2:
    //////////////////////////////////////////////////
    //Stream GCode
    //////////////////////////////////////////////////
    streamGCode();
  }

  // noLoop();
} 


////////////////////////////////////////////////////////////////////////////////////////////////////////

void streamGCode()
{
  lines = loadStrings("bufferGCode.ngc");
  myPort.write(" " + "\n");
  delay(2000);
  isStreamingGcode = true;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////
// DRAWING LINES
void drawPixelLinesH(String thePen)
{
  for (int i = 0; i<pixelLinesH.size(); i++) {
    Line2D myLine = pixelLinesH.get(i);
    stroke(pens.get(thePen));
    line (myLine.a.x, myLine.a.y, myLine.b.x, myLine.b.y);
    if (debug)
    {
      noStroke();
      fill(0, 255, 0);
      ellipse (myLine.a.x, myLine.a.y, 3, 3);
      fill(0, 0, 255);
      ellipse (myLine.b.x, myLine.b.y, 6, 6);
    }
  }
}

void mergePixelLinesH()
{
  for (int n=0; n<iter; n++)
  {
    for (int i = 0; i<pixelLinesH.size(); i++)
    {
      Line2D myLine = pixelLinesH.get(i);
      for (int j= 0; j<pixelLinesH.size(); j++)
      {
        if (i != j)
        {
          Line2D myLine2 = pixelLinesH.get(j);
          if (myLine.b.x == myLine2.a.x && myLine.b.y == myLine2.a.y)
          {
            //Si les points sont identiques, on fusionne les deux
            myLine.b = myLine2.b;
            // On supprime previouLine
            pixelLinesH.remove(j );
          }
        }
      }
    }
  }
}

void drawPixelLinesV(String thePen)
{
  for (int i = 0; i<pixelLinesV.size(); i++)
  {
    Line2D myLine = pixelLinesV.get(i);
    stroke(pens.get(thePen));
    line (myLine.a.x, myLine.a.y, myLine.b.x, myLine.b.y);
    if (debug)
    {
      noStroke();
      fill(255, 0, 0);
      ellipse (myLine.a.x, myLine.a.y, 3, 3);
      fill(0, 255, 0);
      ellipse (myLine.b.x, myLine.b.y, 6, 6);
    }
  }
}

void mergeAllPixelLines()
{
  for (int i = 0; i<pixelLinesMerged.size(); i++)
  {
    ColoredTriangle2D myLine = pixelLinesMerged.get(i);
    Vec2D offsetedPoint1 = new Vec2D (myLine.a.x + myLine.c.x, myLine.a.y + myLine.c.y);
    Vec2D offsetedPoint2 = new Vec2D (myLine.b.x + myLine.c.x, myLine.b.y + myLine.c.y);
    pixelLinesFinal.add(new ColoredLine2D(new Vec2D(offsetedPoint1.x, offsetedPoint1.y), new Vec2D(offsetedPoint2.x, offsetedPoint2.y), myLine.getColor()));
  }
}

void drawAllPixelLines()
{
  for (int i = 0; i<pixelLinesFinal.size(); i++)
  {
    ColoredLine2D myLine = pixelLinesFinal.get(i);
    stroke(pens.get(myLine.getColor()));
    line (myLine.a.x, myLine.a.y, myLine.b.x, myLine.b.y);
  }
}

void mergePixelLinesV()
{
  for (int n=0; n<iter; n++)
  {
    for (int i = 0; i<pixelLinesV.size(); i++)
    {
      Line2D myLine = pixelLinesV.get(i);
      for (int j= 0; j<pixelLinesV.size(); j++)
      {
        if (i != j)
        {
          Line2D myLine2 = pixelLinesV.get(j);
          if (myLine.b.x == myLine2.a.x && myLine.b.y == myLine2.a.y)
          {
            //Si les points sont identiques, on fusionne les deux
            myLine.b = myLine2.b;
            // On supprime previouLine
            pixelLinesV.remove(j );
          }
        }
      }
    }
  }
}

void populatePixelLinesMerged(String color1, String color2)
{
  for (int i = 0; i<pixelLinesV.size(); i++)
  {

    ColoredTriangle2D myLine = new ColoredTriangle2D (pixelLinesV.get(i).a, pixelLinesV.get(i).b, penOffsets.get(color1), color1);
    pixelLinesMerged.add(myLine);
  }
  for (int i = 0; i<pixelLinesH.size(); i++)
  {
    ColoredTriangle2D myLine = new ColoredTriangle2D (pixelLinesH.get(i).a, pixelLinesH.get(i).b, penOffsets.get(color2), color2);
    pixelLinesMerged.add(myLine);
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////

String thePen(String theColor)
{
  String statePen = "";
  if (theColor.equals("red"))
  {
    statePen = "M4 S30";
  } else if (theColor.equals("green"))
  {
    statePen = "M4 S80";
  } else if (theColor.equals("blue"))
  {
    statePen = "M4 S200";
  } else if (theColor.equals("black"))
  {
    statePen = "M4 S255";
  } else if (theColor.equals("up"))
  {
    statePen = "M4 S0";
  }
  return statePen;
}


public void GCodeInit() {
  index = 0;
  System.out.println("Init");
  output.println("G21");
  output.println(thePen("up"));
  output.println("G0" + " " + "F" + motorFeedFast + " " + "X0.0" + " " +  "Y0.0"); 
  output.println(" ");
}

public void GCodeWrite() {

  // export GCODE file
  //convertPixelLinesToPaperLines();
  for (int i=0; i<pixelLinesFinal.size (); i++) {
    // iterate through the saved lines coordinates and write them to the GCode file
    ColoredLine2D currentLine = pixelLinesFinal.get(i);

    if (i == 0) {
      // move from the home/origin to the first point with penUP
      output.println(thePen("up"));
      output.println("G0" + " " + "F" + motorFeedFast);
      output.println("G0" + " " + "X" + currentLine.a.x + " " + "Y" + currentLine.a.y);  

      // this is the first line that gets drawn
      output.println(thePen(currentLine.getColor()));
      isPenDown = true;
      output.println("G0" + " " + "F" + motorFeedSlow);
      output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + currentLine.a.y);
      output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + currentLine.b.y);
    } else {
      // Je supprime la partie qui check si les deux lignes ont un point commun, car l'optimisation est faite avant.
      // the two lines DO NOT share a vertex, so the pen head IS RAISED
      // the pen head is quickly moved to the next vertex
      output.println(thePen("up"));
      output.println("G0" + " " + "F" + motorFeedFast);
      output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + currentLine.a.y);

      // the line is drawn
      output.println(thePen(currentLine.getColor()));
      output.println("G0" + " " + "F" + motorFeedSlow);
      //output.println("G1" + " " + "X" + currentLine.a.x + " " + "Y" + currentLine.a.y);
      output.println("G1" + " " + "X" + currentLine.b.x + " " + "Y" + currentLine.b.y);
    }
  }

  GCodeEnd();
} 



public void GCodeEnd() {
  System.out.println("End");
  // writes a footer with the end instructions for the GCode output file
  output.println(" ");
  // G0 Z90.0
  // G0 X0 Y0 => go home
  // M5 => stop spindle
  // M30 => stop execution
  output.println(thePen("up"));
  //output.println("G0 Z90.0");
  output.println("G0 X0 Y0");  
  //On remonte la feuille de 60cm
  output.println( "G0  Y-600" + "\n");   
  // On redéfinit le Y =0
  output.println("G10 P0 L20 Y0" + "\n");
  //output.println("M30"); 
  // finalize the GCode text file and quits the current Processing Sketch
  output.flush();  // writes the remaining data to the file
  output.close();  // finishes the output file
  println("***************************");
  println("GCODE EXPORTED SUCCESSFULLY");
  println("***************************");
  delay(500);
}


void raz()
{

  pixelLinesH.clear();
  pixelLinesV.clear();
  pixelLinesMerged.clear();
  pixelLinesFinal.clear();
}


void serialEvent(Serial myPort) {
  // read a char from the serial port:
  char inChar = myPort.readChar();
  if (isStreamingGcode && inChar =='k' && previousChar=='o') {
    if (index<lines.length) 
    {
      String[] command = split(lines[index], '\n');
      String joinedCommand = join(command, " ");
      println ("Command is:" + joinedCommand + " --- Index is " + index + "/" +lines.length);
      myPort.write(joinedCommand + "\n");
      index++;
      if (index == lines.length)
      {

        println ("end of stream");
        delay(3000);
        //myPort.write(0x18 + "\n");
        isStreamingGcode=false;
        delay(2000);
        raz();
        state = 0;
      }
    }
  } else {
    print (inChar);
  }
  previousChar = inChar;
}