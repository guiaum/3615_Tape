
#include <Servo.h>

int incomingByte;      // a variable to read incoming serial data into

Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
Servo myservo3;  // create servo object to control a servo
Servo myservo4;  // create servo object to control a servo

int pos1 = 0;//Stylo Levé
int pos2 = 90;//Stylo Baissé
int pos3 = 180;//Stylo Levé
int pos4 = 90;//Stylo Baissé

void setup() {
  // initialize serial communication:
  Serial.begin(9600);
  myservo1.attach(5);
  myservo2.attach(6);
  myservo3.attach(9);
  myservo4.attach(10);
  
}

void loop() {
  // see if there's incoming serial data:
  if (Serial.available() > 0) {
    // read the oldest byte in the serial buffer:
    incomingByte = Serial.read();
    if (incomingByte == 'A') {
      myservo1.write(pos1);
      myservo2.write(pos3);
      myservo3.write(pos1);
      myservo4.write(pos3);
    }
    else if (incomingByte == 'B') {
      myservo1.write(pos2);
      myservo2.write(pos3);
      myservo3.write(pos1);
      myservo4.write(pos3);
    }
    else if (incomingByte == 'C') {
      myservo1.write(pos1);
      myservo2.write(pos4);
      myservo3.write(pos1);
      myservo4.write(pos3);
    }
    else if (incomingByte == 'D') {
      myservo1.write(pos1);
      myservo2.write(pos3);
      myservo3.write(pos2);
      myservo4.write(pos3);
    }
    else if (incomingByte == 'E') {
      myservo1.write(pos1);
      myservo2.write(pos3);
      myservo3.write(pos1);
      myservo4.write(pos4);
    }
  }
}
