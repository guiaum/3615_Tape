char val; // Data received from the serial port
int val1 = 7; // Set the pin to digital I/O 4
int val2 = 8;
int val3 = 9;

void setup() {
  pinMode(7, OUTPUT); // Set pin as OUTPUT
  pinMode(8, OUTPUT); // Set pin as OUTPUT
  pinMode(9, OUTPUT); // Set pin as OUTPUT
  Serial.begin(9600); // Start serial communication at 9600 bps
}

void loop() {
  while (Serial.available()) { // If data is available to read,
    val = Serial.read(); // read it and store it in val
  }
  if (val == 'H') { // If H was received
    digitalWrite(val1, HIGH); // turn the LED on
  } 
  if (val == 'K') {
    digitalWrite(val1, LOW); // Otherwise turn it OFF
  }

  if (val == 'I') { // If H was received
    digitalWrite(val2, HIGH); // turn the LED on
  } 
  if (val == 'L') {
    digitalWrite(val2, LOW); // Otherwise turn it OFF
  }

  if (val == 'J') { // If H was received
    digitalWrite(val3, HIGH); // turn the LED on
  } 
  if (val == 'M') {
    digitalWrite(val3, LOW); // Otherwise turn it OFF
  }

  
  
  delay(100); // Wait 100 milliseconds for next reading
}

