/*
  Reading the values sent from D11 by GRBL, speed of spindle
  Usefull to choose between the pens
*/

int sensorPin = 3;    // select the input pin for the potentiometer

void setup() {
  Serial.begin(9600);
  pinMode(sensorPin, INPUT);
}

void loop() {
  // read the value from the sensor:
  int var = pulseIn(sensorPin, HIGH, 4200);
  if (var == 0 && digitalRead(sensorPin) == 1) {
    var = 2100;
  }
  var = map(var, 0, 2100, 0, 1023);

  if (var < 10)
  {
    Serial.print('A');
  } else if (var >= 10 && var < 80)
  {
    Serial.print('B');
  } else if (var >= 80 && var < 200)
  {
    Serial.print('C');
  } else if (var >= 200 && var < 400)
  {
    Serial.print('D');
  } else if (var >= 400)
  {
    Serial.print('E');
  }
  

}
