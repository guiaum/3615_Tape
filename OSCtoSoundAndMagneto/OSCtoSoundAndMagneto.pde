/**
 * Simple Write. 
 * 
 * Check if the mouse is over a rectangle and writes the status to the serial port. 
 * This example works with the Wiring / Arduino program that follows below.
 */

import oscP5.*;
import netP5.*;
import processing.serial.*;
import processing.sound.*;

float maxVol = 0.15;

OscP5 oscP5;
NetAddress myRemoteLocation;

Serial myPort;  // Create object from Serial class
int val;        // Data received from the serial port

ArrayList <SoundFile> mySamplePlayer = new ArrayList();

int[] states1 = new int[16];
int[] states2 = new int[16];
int[] states3 = new int[16];
int[] refStates = new int[16];
float[] volStates = new float[16];

boolean magneto1 = false;
boolean magneto2 = false;
boolean magneto3 = false;

boolean previousMagneto1 = false;
boolean previousMagneto2 = false;
boolean previousMagneto3 = false;

void setup() 
{
  size(200, 200);
  printArray(Serial.list());
  String portName = "/dev/tty.wchusbserialfa120";
  myPort = new Serial(this, portName, 9600);

  oscP5 = new OscP5(this, 9001);

  //
  for (int i=0; i<16; i++)
  {
    int index = i+1;
    SoundFile soundfile;
    String audioFileName = "3615_Tape_"+index+"m.wav";
    soundfile = new SoundFile(this, audioFileName);
    mySamplePlayer.add(soundfile);
    soundfile.loop();
    soundfile.amp(0);

    //init
    states1[i] = 0;
    states2[i] = 0;
    states3[i] = 0;
    refStates[i] = 0;
    volStates[i] = 0.0;
  }
}

void draw() {
  background(255);
  mix();
  magneto();
}

void magneto()
{
  int total1 = 0;
  int total2 = 0;
  int total3 =0;
  for (int i=0; i<16; i++)
  {
    total1 += states1[i];
    total2 += states2[i];
    total3 += states3[i];
  }
  if (total1>0)
  {
    magneto1 = true;
    if (magneto1 != previousMagneto1)
    {
      myPort.write('H');
      previousMagneto1 = magneto1;
    }
  } else {

    magneto1 = false;
    if (magneto1 != previousMagneto1)
    {
      myPort.write('K');
      previousMagneto1 = magneto1;
    }
  }

  if (total2>0)
  {
    magneto2 = true;
    if (magneto2 != previousMagneto2)
    {
      myPort.write('I');
      previousMagneto2 = magneto2;
    }
  } else {
    magneto2 = false;
    if (magneto2 != previousMagneto2)
    {
      myPort.write('L');
      previousMagneto2 = magneto2;
    }
  }

  if (total3>0)
  {
    magneto3 = true;
    if (magneto3 != previousMagneto3)
    {
      myPort.write('J');
      previousMagneto3 = magneto3;
    }
  } else {
    magneto3 = false;
    if (magneto3 != previousMagneto3)
    {
      myPort.write('M');
      previousMagneto3 = magneto3;
    }
  }
  
}

void mix()
{
  for (int i=0; i<16; i++)
  {
    if ( refStates[i]>0)
    {
      if (volStates[i]<maxVol)
      {
        volStates[i]+=0.01;
      }
    } else
    {
      if (volStates[i]>0.0)volStates[i]-=0.01;
      ;
    }
    mySamplePlayer.get(i).amp(volStates[i]);
  }
}

void keyPressed()
{
  // TEST pur déclencher les magnétos
  //ON
  if (key == 'h')myPort.write('H'); 
  if (key == 'i')myPort.write('I');
  if (key == 'j')myPort.write('J');
  //OFF
  if (key == 'k')myPort.write('K'); 
  if (key == 'l')myPort.write('L');
  if (key == 'm')myPort.write('M');

  if (key == '1')
  {
    mySamplePlayer.get(0).amp(0.15);
    mySamplePlayer.get(1).amp(0.15);
    mySamplePlayer.get(3).amp(0.15);
  } 
  if (key == '0')
  {
    mySamplePlayer.get(0).amp(0);
    mySamplePlayer.get(1).amp(0);
    mySamplePlayer.get(3).amp(0);
  }
}

void oscEvent(OscMessage theOscMessage) {
  /* check if theOscMessage has the address pattern we are looking for. */
  if (theOscMessage.checkAddrPattern("/track")==true) {
    /* check if the typetag is the right one. */
    if (theOscMessage.checkTypetag("is")) {
      /* parse theOscMessage and extract the values from the osc message arguments. */
      int firstValue = theOscMessage.get(0).intValue();  // get the first osc argument
      String secondValue = theOscMessage.get(1).stringValue(); // get the third osc argument
      print("### received an osc message /test with typetag ifs.");
      println(" values: "+firstValue+", "+secondValue);
      proceedSecondValue(firstValue, secondValue);
      return;
    }
  }
  println("### received an osc message. with address pattern "+
    theOscMessage.addrPattern()+" typetag "+ theOscMessage.typetag());
}

void proceedSecondValue(int magneto, String value)
{

  if (magneto == 1)
  {
    convertString(states1, value);
  } else if (magneto == 2)
  {
    convertString(states2, value);
  } else if (magneto == 3)
  {
    convertString(states3, value);
  }
}

void convertString(int[] myStates, String value)
{
  for (int i=0; i<16; i++)
  {
    String myChar = str(value.charAt(i));
    myStates[i] = int(myChar);
  }

  updateRefStates();
}

void updateRefStates()
{
  println("RefStates");
  for (int i = 0; i<16; i++)
  {
    refStates[i] =  states1[i]+states2[i]+states3[i];
    print(refStates[i]);
  }
  //mix();
}